package inc.minuth.homework5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;
import inc.minuth.homework5.adapter.WebAdapter;
import inc.minuth.homework5.callback.WebCallBack;
import inc.minuth.homework5.customdialog.WebInputDialog;
import inc.minuth.homework5.model.Web;

public class MainActivity extends AppCompatActivity implements WebCallBack {

    List<Web> webList;
    WebAdapter webAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RecyclerView recyclerView=findViewById(R.id.recyclerView);
        webList=new ArrayList<>();
        webAdapter=new WebAdapter(this,webList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(webAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.add_menu:
                WebInputDialog dialog=new WebInputDialog();
                dialog.show(getSupportFragmentManager(),"INPUT");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void add(Web web) {
        webList.add(web);
        webAdapter.notifyDataSetChanged();
    }

    @Override
    public void delete(Web web) {
        webList.remove(web);
        webAdapter.notifyDataSetChanged();
    }

    @Override
    public void update(Web web, int pos) {

    }
}
