package inc.minuth.homework5.callback;

import inc.minuth.homework5.model.Web;

public interface WebCallBack
{
    void add(Web web);
    void delete(Web web);
    void update(Web web,int pos);
}
