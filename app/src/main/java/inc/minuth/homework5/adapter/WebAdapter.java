package inc.minuth.homework5.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import inc.minuth.homework5.R;
import inc.minuth.homework5.callback.WebCallBack;
import inc.minuth.homework5.model.Web;

public class WebAdapter extends RecyclerView.Adapter<WebAdapter.ViewHolder>
{
    private LayoutInflater inflater;
    private List<Web>webList;
    private WebCallBack webCallBack;
    private Context context;
    public WebAdapter(Context context, List<Web> webList) {
        this.webList = webList;
        this.context=context;
        this.inflater=LayoutInflater.from(context);
        webCallBack=(WebCallBack)context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view=inflater.inflate(R.layout.list_data, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Web web=getItem(i);
        viewHolder.tvWebUrl.setText(web.getWebUrl());
        viewHolder.tvPhone.setText(web.getPhone());
        viewHolder.tvName.setText(web.getName());
        viewHolder.tvEmail.setText(web.getEmail());
        viewHolder.tvAddress.setText(web.getAddress());
        Uri imgUri=Uri.parse(web.getImagePath());
        viewHolder.imgSubLogo.setImageURI(imgUri);
        viewHolder.imgLogo.setImageURI(imgUri);
    }


    @Override
    public int getItemCount() {
        return webList.size();
    }
    public Web getItem(int i)
    {
        return webList.get(i);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgLogo;
        ImageView imgSubLogo;
        TextView tvName;
        TextView tvPhone;
        TextView tvEmail;
        TextView tvWebUrl;
        TextView tvAddress;
        Button btnDelete;
        public ViewHolder(@NonNull final View view) {
            super(view);
            imgLogo=view.findViewById(R.id.imgView);
            imgSubLogo=view.findViewById(R.id.imgLogo);
            tvAddress=view.findViewById(R.id.tvAddress);
            tvEmail=view.findViewById(R.id.tvMail);
            tvName=view.findViewById(R.id.tvName);
            tvPhone=view.findViewById(R.id.tvPhone);
            tvWebUrl=view.findViewById(R.id.tvWeb);
            btnDelete=view.findViewById(R.id.btnDelete);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int index=getAdapterPosition();
                    webCallBack.delete(webList.get(index));
                }
            });
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Web web=webList.get(getAdapterPosition());
                    Intent intent=new Intent(Intent.ACTION_VIEW,Uri.parse(web.getWebUrl()));
                    context.startActivity(intent);
                }
            });

        }
    }
}
