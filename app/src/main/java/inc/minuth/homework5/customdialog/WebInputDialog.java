package inc.minuth.homework5.customdialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import inc.minuth.homework5.R;
import inc.minuth.homework5.callback.WebCallBack;
import inc.minuth.homework5.model.Web;

public class WebInputDialog extends DialogFragment
{
    ViewHolder viewHolder;
    Uri uri;
    Web web;
    public void setData(Web web)
    {
        this.web=web;
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(getContext());
        final View view=getActivity().getLayoutInflater().inflate(R.layout.web_input,null);
        viewHolder=new ViewHolder(view);
        if(web!=null)
        {
            viewHolder.imageView.setImageURI(uri.parse(web.getImagePath()));
            viewHolder.edtWebUrl.setText(web.getImagePath());
            viewHolder.edtPhone.setText(web.getPhone());
            viewHolder.edtName.setText(web.getName());
            viewHolder.edtEmail.setText(web.getEmail());
            viewHolder.edtAddress.setText(web.getAddress());
        }
        viewHolder.btnBrowse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,1);
            }
        });
        builder.setTitle("Add new university");
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                WebCallBack callBack=(WebCallBack)getActivity();
                Web web=new Web();
                web.setName(viewHolder.edtName.getText().toString());
                web.setAddress(viewHolder.edtAddress.getText().toString());
                web.setEmail(viewHolder.edtEmail.getText().toString());
                web.setPhone(viewHolder.edtPhone.getText().toString());
                web.setWebUrl(viewHolder.edtWebUrl.getText().toString());
                web.setImagePath(uri.toString());
                callBack.add(web);
            }
        });
        builder.setView(view);
        return builder.create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1&&resultCode== Activity.RESULT_OK)
        {
            uri=data.getData();
            viewHolder.imageView.setImageURI(uri);
        }
    }

    class ViewHolder
    {
        EditText edtPhone;
        EditText edtEmail;
        EditText edtWebUrl;
        EditText edtAddress;
        EditText edtName;
        Button btnBrowse;
        ImageView imageView;
        public ViewHolder(View view)
        {
            edtAddress = view.findViewById(R.id.edtAddress);
            edtEmail = view.findViewById(R.id.edtEmail);
            edtName = view.findViewById(R.id.edtName);
            edtPhone = view.findViewById(R.id.edtPhone);
            edtWebUrl = view.findViewById(R.id.edtWebUrl);
            btnBrowse=view.findViewById(R.id.btnBrowseImage);
            imageView=view.findViewById(R.id.imgView);
        }
    }
}
